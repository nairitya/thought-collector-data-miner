from db import database
from download import downloader
from regex import combined
from data import management

import config
import thread, time

class Worker:

	def __init__(self):
		self.configuration = config.config()
		self.database = database.Database(self.configuration)
		self.data_extract = combined.Combine()
		self.download = downloader.Downloader(management.manage())
		self.url = self.download.taken_from()

	def start(self):
		# thread.start_new_thread(self.download.start_self, ())
		# thread.start_new_thread()
		# self.download.start_self()
		f = open("data/saved_pages/1.html")
		d = f.read()
		f.close()
		self.data_extract.parser(d, self.database, self.url)

	"""
	A function start should run three process
		1. Downloader
		2. Regex
		3. Database
	in three different threads!
	"""
