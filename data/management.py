# -*- coding: utf-8 -*-
import os

class manage:

	def __init__(self):

		self.folder = os.path.join(os.path.dirname(__file__), 'saved_pages')

	def save_page(self, html, counter):

		"""
		define the location to save the file.
		"""
		location = os.path.join(self.folder, str(counter)+".html")

		"""
		save the file in this location.
		"""
		f = open(location, "w")
		f.write(html.replace("&ldquo;", "").replace("&#8213;","").replace("&rdquo;","").encode('utf-8'))
		f.close()

		return True

	def remove_page(self, counter):

		"""
		delete the file. Because it has been processed.
		"""
		location = os.path.join(self.folder, str(counter)+".html")
		os.remove(location)
