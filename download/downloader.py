# -*- coding: utf-8 -*-
import requests

class Downloader:

	def __init__(self, data_path):

		"""
		url from we are mining data
		"""
		self.url = "http://www.goodreads.com/quotes"

		"""
		we will start from page 1.
		"""
		self.counter = 1

		"""
		setting data path
		"""
		self.data_path = data_path

	"""
	function to get html from url
	"""
	def get_page(self):

		"""
		sending a get request to url with page number counter.
		"""

		data = {
			"page": self.counter
		}
		print "Downloading page %s?page=%d" % (self.url, self.counter)
		page = requests.get(self.url, params=data)

		"""
		if the request is successful then proceed
		"""
		if(page.status_code == 200):
			self.data_path.save_page(page.text, self.counter)
			self.counter += 1

	"""
	function to start the downloader
	"""
	def start_self(self):

		"""
		we will consider only first 100 pages !!
		"""
		count = 100

		while self.counter < count:
			self.get_page()

	def taken_from(self):
		return self.url
