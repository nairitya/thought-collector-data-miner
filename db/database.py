import psycopg2
from datetime import datetime

"""
CREATE TABLE IF NOT EXISTS thoughts (
                    id bigserial primary key,
                    thought varchar(2000) NOT NULL,
                    author varchar(200) NOT NULL,
                    owner  varchar(200) NOT NULL,
                    created_at timestamp default NULL,
                    updated_at timestamp default NULL
                );
"""
class Database:

	"""
	feed it config to proceed
	"""
	def __init__(self, config):

		self.host = config['host']
		self.user = config['username']
		self.password = config['password']
		self.database = config['database']
		self.connect()

	def connect(self):

		self.connection = psycopg2.connect(
			host=self.host,
			user=self.user,
			password=self.password,
			database=self.database
		)

	"""
	def populate(self, author, thought, taken_from):

		date_added = datetime.now()
		query = """
		# INSERT INTO thoughts(thought, author, owner, created_at, updated_at) 
		# VALUES (\'%s\', \'%s\', \'%s\', \'%s\', \'%s\');
		""" % (thought.replace("'", "'"), author, taken_from, date_added, date_added)

		print query
		connection = self.connection
		cur = connection.cursor()
		cur.execute(query)
		connection.commit()
		cur.close()
	"""

	#source :- http://wiki.postgresql.org/wiki/Psycopg2_Tutorial
	def multiple_population(self, authors, thoughts, taken_from):

		arr = []
		date_added = datetime.now()
		# print len(authors), len(thoughts)
		for i in range(0, len(thoughts)):
			arr.append({"thought": thoughts[i], "author": authors[i], "owner": taken_from, "created_at": date_added, "updated_at": date_added})

		query = tuple(arr)
		connection = self.connection
		cur = connection.cursor()
		cur.executemany("""INSERT INTO thoughts (thought, author, owner, created_at, updated_at) VALUES (%(thought)s, %(author)s, %(owner)s, %(created_at)s, %(updated_at)s)""", query)
		connection.commit()
		cur.close()