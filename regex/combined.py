import authors
import thoughts

class Combine:

	def __init__(self):
		self.author = authors.author()
		self.thought = thoughts.thought()
		pass

	def parser(self, html, db, taken_from):
		author_name = self.author.get_name(html)
		thought_text = self.thought.get_thought(html)
		db.multiple_population(author_name, thought_text, taken_from)

