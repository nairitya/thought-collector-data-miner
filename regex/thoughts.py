# -*- coding: utf-8 -*-
import re

class thought:

	def __init__(self):

		"""
		the regex which matches with the thought!
		"""
		self.reg = r'<div class="quoteText">[\n\t ]*[A-Za-z0-9\n\t .\'\",\”\“]*<br>'
		self.regex = re.compile(self.reg)

		"""
		unwanted text coming with the regex!
		"""
		self.remove = ['<div class="quoteText">', '<br>', '\n', '\t']

	def clean(self, text):

		"""
		remove all the unwanted variables
		"""
		for noise in self.remove:
			text = text.replace(noise, "")

		"""
		returns the clean text
		"""
		return text

	def get_thought(self, text):

		thoughts = self.regex.findall(text)
		thought_text = []

		for thought in thoughts:
			thought_text.append(self.clean(thought).strip())

		return thought_text
