# -*- coding: utf-8 -*-
import re

class author:

	def __init__(self):

		"""
		the regex which matches with the author!
		"""
		self.reg = r'<a href="/author/show/[A-Za-z0-9\n\t .\'\",\”\“_]+">[A-Za-z0-9.\_ \t,]+</a>'
		self.regex = re.compile(self.reg)

		"""
		the regex which matches with the author_name in author(above) regex!
		"""
		self.reg_name = r'>[A-Za-z0-9\n\t .\'\",\”\“_]+<'
		self.regex_name = re.compile(self.reg_name)

		"""
		unwanted text coming with the regex!
		"""
		self.remove = ['>', '<']

	def clean(self, text):

		"""
		remove all the unwanted variables
		"""
		for noise in self.remove:
			text = text.replace(noise, "")

		"""
		returns the clean text
		"""
		return text

	def get_name(self, text):
		initial = self.regex.findall(text)
		names = []

		for name in initial:

			"""
			returns one name! like ['>Nairitya Khilari<']
			"""
			ex = self.regex_name.findall(name)

			names.append(self.clean(ex[0]))

		return names
